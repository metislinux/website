+++
title = "Metis Linux Thanks Page"
description = "Metis Linux Thanks Page - Huge thanks and respect to each and every helping hands who made possible to create metis-os!"
page_template = "page.html"
+++
Huge thanks and respect to each and every helping hands who made possible to create metis-os!
<!-- more -->

<hr>
<center><h3>Core Team</h3></center>
<div class="thanks">
<div class="eachone" id="down">
<a href="https://codeberg.org/whoisYoges" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/core/yogesh.jpg" alt="core team member">
<p>Yogesh Lamichhane</p>
</a>
</div>
<div class="eachone" id="up">
<a href="https://github.com/pwnwriter" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/core/nabeen.jpg" alt="core team member">
<p>Nabeen Tiwaree</p>
</a>
</div>
<div class="eachone" id="down">
<a href="https://github.com/mrsafalpiya" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/core/safal.jpg" alt="core team member">
<p>Safal Piya</p>
</a>
</div>
</div>
<center><h4>Helpful Hands</h4></center>
<div class="thanks">
<div class="eachone">
<a href="https://artixlinux.org" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/artix.png" alt="helpful hand">
<p>Artix Linux</p>
</a>
</div>
<div class="eachone">
<a href="https://suckless.org" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/sucklessdotorg.png" alt="helpful hand">
<p>Suckless.org</p>
</a>
</div>
<div class="eachone">
<a href="https://github.com/dylanaraps" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/dylan.png" alt="helpful hand">
<p>Dylan Araps</p>
</a>
</div>
<div class="eachone">
<a href="https://github.com/lukesmithxyz" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/lukesmith.png" alt="helpful hand">
<p>Luke Smith</p>
</a>
</div>
<div class="eachone">
<a href="https://github.com/bruenig" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/bruenig.png" alt="helpful hand">
<p>Matthew Bruenig</p>
</a>
</div>
<div class="eachone">
<a href="https://github.com/fairyglade/ly" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/fairyglade.png" alt="helpful hand">
<p>Fairy Glade</p>
</a>
</div>
<div class="eachone">
<a href="https://instagram.com/loneillustrator" target="_blank" rel="noopener noreferrer">
<img src="/assets/images/pages/thanks/cool/nirdesh.png" alt="helpful hand">
<p>Lone Illustrator</p>
</a>
</div>
</div>
<hr>
<div class="home">
<a href="/" title="Metis Home">Back</a>
</div>