+++
title = "Download Metis Linux"
description = "Download and try Metis Linux - Systemd-less, super smooth, ultra minimalistic, resource efficient, highly customizable and absolute performance oriented operating system!"
page_template = "page.html"
[extra]
slideshow = true
+++
Download Metis Linux and replace your current bloated resorce hungry operating system whether it's Windows or macOS or run Metis Linux alongside it.
<!-- more -->

<hr>
<div class="features">

### Ready To Download?

Get the systemd-less, super smooth, ultra minimalistic, resource efficient, highly customizable and absolute performance oriented operating system for super productivity.

<div id="slider">
    <div id="mask">
        <ul>
            <li class="first"><img src="/assets/images/pages/download/metis-linux-preview-1.jpg"/></li>
            <li class="second"><img src="/assets/images/pages/download/metis-linux-preview-2.jpg"/></li>
            <li class="third"><img src="/assets/images/pages/download/metis-linux-preview-3.jpg"/></li>
            <li class="fourth"><img src="/assets/images/pages/download/metis-linux-preview-4.jpg"/></li>
            <li class="fifth"><img src="/assets/images/pages/download/metis-linux-preview-5.jpg"/></li>
        </ul>
        <div class="progress-bar"></div>
    </div>
</div>


<p class="installation-guide">
    <a href="https://github.com/metis-os/metis-iso/releases/download/v1.1.1/metis-base-runit-20220717-v1.1.1-x86_64.iso" target="_blank" rel="noopener noreferrer"><button class="btn-installation-guide">Download Now</button></a>
</p>

##### Alternative Download Links!

- <https://github.com/metis-os/metis-iso/releases>

### Check our installation guide for detailed installation instructions.

<p class="installation-guide">
    <a href="/installation-guide"><button class="btn-installation-guide"> Installation Guide</button></a>
</p>

*note:* You may not get full performance if you're installing metis linux in a virtual machine!

Yes! Metis Linux is Completely Free and open source. Want to [check it's source code](https://github.com/metis-os)?  
Follow us on [GitHub](https://github.com/metis-os).

<hr>
<div class="home">
    <center>
        <a href="/" title="Metis Home">Back</a>
        <a href="/key-bindings" title="Default Key Bindings in Metis">Key-Bindings</a>
    </center>
</div>