+++
title = "Key Bindings in Metis Linux"
description = "Key Bindings of Metis Linux - list of default keybindings used in metis linux!"
page_template = "page.html"
+++
The key bindings are defined in the window manager itself ([dwm](https://github.com/metis-os/dwm)) which is configured in its config file (config.h).  
<!-- more -->
<hr>

*Note:* MOD is super (windows) key

#### Applications

<p><span>• MOD + c:</span> Close currently focused client</p>
<p><span>• MOD + Space:</span> Open dmenu prompt</p>
<p><span>• MOD + Enter:</span> Open ST</p>

#### Window management

<p><span>• MOD + l:</span> Increase master area</p>
<p><span>• MOD + h:</span> Decrease master area</p>
<p><span>• MOD + k:</span> Change focus to the window lower in stack</p>
<p><span>• MOD + j:</span> Change focus to the window higher in stack</p>
<p><span>• MOD + f:</span> Promote currently selected window to the top of the stack</p>
<p><span>• MOD + d:</span> Decrease master count by one</p>
<p><span>• MOD + s:</span> Increase master count by one</p>
<p><span>• MOD + r:</span> Toggle mouse-resize mode</p>

#### Layouts

<p><span>• MOD + z:</span> Change layout to tiling (Default)</p>
<p><span>• MOD + e:</span> Change layout to floating</p>
<p><span>• MOD + x:</span> Change layout to monocle</p>
<p><span>• MOD + t:</span> Toggle last two layouts</p>
<p><span>• MOD + Shift + w:</span> Toggle floating on currently focused window</p>
<p><span>• MOD + Minus:</span> Decrease gaps between windows </p>
<p><span>• MOD + Plus:</span> Increase gaps between windows</p>
<p><span>• MOD + Shift + Plus:</span> Reset the gaps between windows</p>

#### Tags

<p><span>• MOD + 1 . . . 5:</span> Switch tags</p>
<p><span>• MOD + Tab:</span> Toggle between last two tags</p>
<p><span>• MOD + 0:</span> View all tags</p>
<p><span>• MOD + Shift + 0:</span> Tag currently focused window to all</p>

#### Monitors

<p><span>• MOD + comma:</span> Change focus to the monitor lower in stack</p>
<p><span>• MOD + period:</span> Change focus to the monitor higher in stack</p>
<p><span>• MOD + Shift +comma:</span> Move currently focused window to the monitor lower in stack</p>
<p><span>• MOD + Shift + period:</span> Move currently focused window to the monitor higher in stack</p>

#### Scratchpads

<p><span>• MOD + y:</span> Toggle one st scratchpad terminal</p>
<p><span>• MOD + u:</span> Toggle second st scratchpad terminal</p>

#### Others

<p><span>• MOD + g:</span> Toggle bar</p>
<p><span>• MOD + Shift + q:</span> Exit dwm</p>
<p><span>• MOD + Control + Shift + q:</span> Restart dwm</p>
<p><span>• MOD + shift + s:</span> Poweroff/Reboot selection menu </p>

[Get offline version of Key Bindings Pdf!](/assets/images/pdfs/metis-dwm-keybinds.pdf)

<div class="home">
    <hr>
    <a href="/" title="Metis Home">Back</a>
</div>