+++
title = "Contribute To Metis Linux"
description = "If you like metis linux and it's concept, help metis linux to grow contributing and donating any kind of physical, digital or financial assets!"
page_template = "page.html"
+++
Liked metis linux and it's idea? You can support us by any means!
<!-- more -->

<hr>
<div class="features">

There are many ways to contribute to Metis Linux. You are anytime welcomed to be a part of metis-os.

### How And Where To Contribute

#### Improve and Add Documentations

You're welcomed to improve and documentations wherever necessary on our [website](https://metislinux.org) and [git repos](https://github.com/metis-os).

#### Translations into different languages

You could help us translating our websites and repos into other different languages so that it would reach to more people.

#### Creation of different arts

You can create different arts such as wallpapers, pictures, videos and other related to metis linux.

#### Contribute to code

You can contribute us with actual codes of the iso. Update improve and add features to the system.

#### Maintain and host metis repository server

You can help us maintain our repos hosting in your servers. Or, you can contribute maintaining our own servers as well.

#### Sponsors and Donations

Anyone could [sponsor or donate us](https://donate.metislinux.org) any kind of physical, digital or financial assets.

#### Share us

Yes! You could share our projects among people that would make more people know about the project.

#### Queries, Suggestions, Advice, Problems and Bugs

Leave your queries, suggestions, problems and bugs to our [github issue section](https://github.com/metis-os/reports/issues) so that we could update and improve them for you.

<hr>
<div class="home">
    <p> <a href="/" title="Home Page">Back</a> </p>
</div>