+++
title = "About Metis Linux"
description = "Metis linux is an artix linux based systemd-less, super smooth, ultra minimalistic, resource efficient, highly customizable and absolute performance oriented operating system for chads!"
page_template = "page.html"
+++
Metis Linux is an Artix Linux based OS, started as a hobby project by a 17 y/o Nepalese teen [Nabeen Tiwaree](https://github.com/pwnwriter) with his fellow friends [Yogesh Lamichhane](https://github.com/whoisYoges) and [Safal Piya](https://github.com/mrsafalpiya).
<!-- more -->

<hr>
<div class="features">

### Features

<p>
    <span>Fast and lightweight:</span> Suckless's simple packages are used wherever possible. Using DWM with only the absolute required packages makes the operating system super minimalistic and only uses about 250 MBs of memory at boot that makes the OS super productive.
</p>
<p>
    <span>Simple and Free:</span> If anything can be simple, let's just simplify it. Our implementations should be written from shell and C/C++, and we love usable and simple package implementations, which are all freely available in <a href="https://github.com/metis-os" target="_blank" rel="noopener noreferrer" title="GitHub of Metis Linux">Github</a>.
</p>
<p>
    <span>systemd-less:</span> Stated by the unix philosophy, <strong>Make each program do one thing well</strong>, systemd includes many features that we never use; making the init system complicated. So <strong>RunIt</strong> is our init system of choice.
</p>
<p>
    <span>POSIX compatible:</span> We want our project can be easily portable to other operating systems.
</p>
</div>

#### Metis Linux ships with following packages by default.

<div class="container">
    <div class="text">
        <ul class="about">
            <li>Text Editor: nvim (Neo-VIM)</li>
            <li>Window Manager: DWM (Dynamic Window Manager)</li>
            <li>Terminal: ST (suckless terminal or simple termimnal)</li>
            <li>Screenshot utility: Scrot</li>
            <li>Bar: SlStatus</li>
            <li>Application Launcher: Dmenu</li>
            <li>brillo: Backlight (brightness) controller</li>
            <li>Pulseaudio: Audio Server for linux <span>(will switch to pipewire on next release)</span></li>
            <li>JetBrains Mono Nerd Font</li>
            <li>Firefox as default browser</li>
        </ul>
    </div>
    <div class="main-image">
        <img src="/assets/images/gifs/super_minimal.gif" alt="wooh! super minimalistic and cool" />
    </div>
</div>
<hr>
<div class="home">
    <center>
        <a href="/" title="Metis Home">Back</a>
        <a href="/download" title="Download Metis Linux">Download</a>
    </center>
</div>
 