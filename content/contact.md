+++
title = "Contact Metis Linux Team"
description = "Contact Metis Linux - Get in touch with the metis linux developers through different mediums!"
page_template = "page.html"
+++
Have any questions/queries/suggestions or wanna do a casual talk?
<!-- more -->
<hr>

Connect with us on any of the following!  
We Would be more than happy to...

<ul class="links">
    <li>
        Twitter: <a href="https://twitter.com/metislinux" target="_blank" rel="noopener noreferrer">@metislinux</a>
    </li>
    <li>
        GitHub: <a href="https://github.com/metis-os" target="_blank" rel="noopener noreferrer">@metis-os</a>
    </li>
    <li>
        Telegram: <a href="https://t.me/metislinux" target="_blank" rel="noopener noreferrer">t.me/metislinux</a>
    </li>
    <li>
        Youtube: <a href="https://youtube.com/@metislinux" target="_blank" rel="noopener noreferrer">@metislinux</a>
    </li>
    <li>
        Email: <a href="mailto:info@metislinux.org" target="_blank" rel="noopener noreferrer">info [at] metislinux [dot] org</a>
    </li>
</ul>

<hr>
<div class="home">
    <a href="/" title="Metis Home">Back</a>
</div>